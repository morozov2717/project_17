const http = require('http');
const fs = require('fs');
const path = require('path');

const PORT = 3000;

const data = fs.readFileSync(path.resolve(__dirname, 'textFolder', 'text.txt'),'utf8')     

const server = http.createServer((req, res)=>{
    console.log('Url:' + req.url);
    res.setHeader('Content-Type', 'text/html')
    res.write(data, 'UTF-8');
    res.end();
});

server.listen(PORT, 'localhost', (err)=>{
    err ? console.log(err) : console.log(`listening port ${PORT}`);
});





